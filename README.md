# Azure Service Bus sessions test

[Azure Service Bus properties](https://learn.microsoft.com/en-us/azure/developer/java/spring-framework/spring-cloud-stream-support?tabs=SpringCloudAzure4x#configuration-1)

# Usage

Create a file `src/main/resources/application-cxnstring.yml`:

```
spring:
  cloud:
    azure:
      credential:
        client-id: XX
        client-secret: XX
      profile:
        tenant-id: XX
      servicebus:
        namespace: XX
```

Start the application with the `cxnstring` Spring profile and activate asyncReceive v2:

```
export com.azure.messaging.servicebus.session.processor.asyncReceive.v2=true
export SPRING_PROFILES_ACTIVE=cnxstring
```

Wait for an `error` log message.


# Process

The application publish/consummes messages under the `testbma.session` topic.

This application produces messages at a regular rate with 3 session IDs:
* 1 message for sessions #0 and #2
* 2 messages for sessions #1, #3, #4 and #5

every 2 seconds.

There are 3 Subscriptions:

| Subscription                              | session-enabled | max-concurrent-sessions | max-concurrent-calls |
|-------------------------------------------|-----------------|-------------------------|----------------------|
| `A_ordered_sub_with_session_1_concurrent` | true            | 1                       | _default_            |
| `B_ordered_sub_with_session_2_concurrent` | true            | 2                       | _default_            |
| `C_ordered_sub_with_session_2_consumers`  | true            | 2                       | 2                    |
| `D_ordered_sub_with_session_3_consumers`  | true            | 3                       | 3                    |

The consumers of the `C_ordered_sub_with_session_2_consumers` and `D_ordered_sub_with_session_3_consumers` subscription wait 500ms before returning.

It is expected that this consumer is **not** call twice, in different threads, for the same session ID. We cannot guarantee that these messages will be treated in a FIFO manner.

A message is logged if it is the case.


# Log example

## Multi-threading problem with more than 1 concurrent call

``` 
12:46:54.539 boundedElastic-4     DEBUG ConsumerSubC: Message #2 received with session '1002'
12:46:54.633 boundedElastic-5     DEBUG ConsumerSubC: Message #4 received with session '1001'
12:46:55.052 boundedElastic-4     DEBUG ConsumerSubC: Message #2 consumed with session '1002'
12:46:55.052 boundedElastic-4     DEBUG ConsumerSubC: Message #3 received with session '1001'
12:46:55.053 boundedElastic-4     ERROR ConsumerSubC: Already processing a message for session {1000=false, 1001=true, 1002=false}
12:46:55.144 boundedElastic-5     DEBUG ConsumerSubC: Message #4 consumed with session '1001'
12:46:55.565 boundedElastic-4     DEBUG ConsumerSubC: Message #3 consumed with session '1001'
```

We can see that the `C` consummer is called for 2 messages of the `1001` session ID, by the `boundedElastic-5` and `boundedElastic-4` threads, at the same time.

## Ordering problem, even if we add a synchro

```
12:34:06.308 boundedElastic-7     DEBUG ConsumerSubD: Message #1 received with session '1000'
12:34:06.368 boundedElastic-8     DEBUG ConsumerSubD: Message #8 received with session '1004'
12:34:06.515 boundedElastic-9     DEBUG ConsumerSubD: Message #10 received with session '1005'
12:34:06.822 boundedElastic-7     DEBUG ConsumerSubD: Message #1 consumed with session '1000'
12:34:06.884 boundedElastic-8     DEBUG ConsumerSubD: Message #8 consumed with session '1004'
12:34:06.884 boundedElastic-7     DEBUG ConsumerSubD: Message #7 received with session '1004'
12:34:06.884 boundedElastic-7     ERROR ConsumerSubD: Bad order: received message #7 before #8
12:34:07.021 boundedElastic-9     DEBUG ConsumerSubD: Message #10 consumed with session '1005'
12:34:07.022 boundedElastic-8     DEBUG ConsumerSubD: Message #9 received with session '1005'
12:34:07.022 boundedElastic-8     ERROR ConsumerSubD: Bad order: received message #9 before #10
12:34:07.389 boundedElastic-7     DEBUG ConsumerSubD: Message #7 consumed with session '1004'
```

There is a race condition before our consumer gets called.

# Maven dependencies

```
 eu.ima.testasb:asb-session-test-sb3:jar:1.0.0-SNAPSHOT
 +- org.springframework.cloud:spring-cloud-stream:jar:4.1.0:compile
 |  +- org.springframework.boot:spring-boot-starter-validation:jar:3.2.2:compile
 |  |  +- org.springframework.boot:spring-boot-starter:jar:3.2.2:compile
 |  |  |  +- org.springframework.boot:spring-boot:jar:3.2.2:compile
 |  |  |  +- org.springframework.boot:spring-boot-starter-logging:jar:3.2.2:compile
 |  |  |  |  +- ch.qos.logback:logback-classic:jar:1.4.14:compile
 |  |  |  |  |  \- ch.qos.logback:logback-core:jar:1.4.14:compile
 |  |  |  |  +- org.apache.logging.log4j:log4j-to-slf4j:jar:2.21.1:compile
 |  |  |  |  |  \- org.apache.logging.log4j:log4j-api:jar:2.21.1:compile
 |  |  |  |  \- org.slf4j:jul-to-slf4j:jar:2.0.11:compile
 |  |  |  +- jakarta.annotation:jakarta.annotation-api:jar:2.1.1:compile
 |  |  |  \- org.yaml:snakeyaml:jar:2.2:compile
 |  |  +- org.apache.tomcat.embed:tomcat-embed-el:jar:10.1.18:compile
 |  |  \- org.hibernate.validator:hibernate-validator:jar:8.0.1.Final:compile
 |  |     +- org.jboss.logging:jboss-logging:jar:3.5.3.Final:compile
 |  |     \- com.fasterxml:classmate:jar:1.6.0:compile
 |  +- org.springframework:spring-messaging:jar:6.1.3:compile
 |  |  +- org.springframework:spring-beans:jar:6.1.3:compile
 |  |  \- org.springframework:spring-core:jar:6.1.3:compile
 |  |     \- org.springframework:spring-jcl:jar:6.1.3:compile
 |  +- org.springframework.integration:spring-integration-core:jar:6.2.1:compile
 |  |  +- org.springframework:spring-aop:jar:6.1.3:compile
 |  |  +- org.springframework:spring-context:jar:6.1.3:compile
 |  |  |  \- org.springframework:spring-expression:jar:6.1.3:compile
 |  |  +- org.springframework:spring-tx:jar:6.1.3:compile
 |  |  +- io.projectreactor:reactor-core:jar:3.6.2:compile
 |  |  |  \- org.reactivestreams:reactive-streams:jar:1.0.4:compile
 |  |  \- io.micrometer:micrometer-observation:jar:1.12.2:compile
 |  |     \- io.micrometer:micrometer-commons:jar:1.12.2:compile
 |  +- org.springframework.integration:spring-integration-jmx:jar:6.2.1:compile
 |  +- org.springframework.retry:spring-retry:jar:2.0.5:compile
 |  +- org.springframework.cloud:spring-cloud-function-context:jar:4.1.0:compile
 |  |  +- net.jodah:typetools:jar:0.6.2:compile
 |  |  +- org.springframework.boot:spring-boot-autoconfigure:jar:3.2.2:compile
 |  |  +- org.springframework.cloud:spring-cloud-function-core:jar:4.1.0:compile
 |  |  \- com.fasterxml.jackson.core:jackson-databind:jar:2.15.3:compile
 |  |     +- com.fasterxml.jackson.core:jackson-annotations:jar:2.15.3:compile
 |  |     \- com.fasterxml.jackson.core:jackson-core:jar:2.15.3:compile
 |  \- org.jetbrains.kotlin:kotlin-stdlib-jdk8:jar:1.9.22:compile
 |     +- org.jetbrains.kotlin:kotlin-stdlib:jar:1.9.22:compile
 |     |  \- org.jetbrains:annotations:jar:13.0:compile
 |     \- org.jetbrains.kotlin:kotlin-stdlib-jdk7:jar:1.9.22:compile
 +- com.azure.spring:spring-cloud-azure-stream-binder-servicebus:jar:5.9.1:compile
 |  +- com.azure.spring:spring-cloud-azure-stream-binder-servicebus-core:jar:5.9.1:compile
 |  |  +- com.azure.spring:spring-integration-azure-servicebus:jar:5.9.1:compile
 |  |  |  +- com.azure.spring:spring-integration-azure-core:jar:5.9.1:compile
 |  |  |  |  \- com.azure.spring:spring-messaging-azure:jar:5.9.1:compile
 |  |  |  \- com.azure.spring:spring-messaging-azure-servicebus:jar:5.9.1:compile
 |  |  \- com.azure:azure-messaging-servicebus:jar:7.15.0:compile
 |  |     +- com.azure:azure-core:jar:1.45.1:compile
 |  |     |  +- com.azure:azure-json:jar:1.1.0:compile
 |  |     |  +- com.fasterxml.jackson.datatype:jackson-datatype-jsr310:jar:2.15.3:compile
 |  |     |  \- org.slf4j:slf4j-api:jar:2.0.11:compile
 |  |     +- com.azure:azure-core-amqp:jar:2.9.0:compile
 |  |     |  +- com.microsoft.azure:qpid-proton-j-extensions:jar:1.2.4:compile
 |  |     |  \- org.apache.qpid:proton-j:jar:0.33.8:compile
 |  |     +- com.azure:azure-core-http-netty:jar:1.13.11:compile
 |  |     |  +- io.netty:netty-handler:jar:4.1.105.Final:compile
 |  |     |  |  +- io.netty:netty-resolver:jar:4.1.105.Final:compile
 |  |     |  |  \- io.netty:netty-transport:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-handler-proxy:jar:4.1.105.Final:compile
 |  |     |  |  \- io.netty:netty-codec-socks:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-buffer:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-codec:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-codec-http:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-codec-http2:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-transport-native-unix-common:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-transport-native-epoll:jar:linux-x86_64:4.1.105.Final:compile
 |  |     |  |  \- io.netty:netty-transport-classes-epoll:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-transport-native-kqueue:jar:osx-x86_64:4.1.105.Final:compile
 |  |     |  |  \- io.netty:netty-transport-classes-kqueue:jar:4.1.105.Final:compile
 |  |     |  +- io.netty:netty-tcnative-boringssl-static:jar:2.0.61.Final:compile
 |  |     |  |  +- io.netty:netty-tcnative-classes:jar:2.0.61.Final:compile
 |  |     |  |  +- io.netty:netty-tcnative-boringssl-static:jar:linux-x86_64:2.0.61.Final:compile
 |  |     |  |  +- io.netty:netty-tcnative-boringssl-static:jar:linux-aarch_64:2.0.61.Final:compile
 |  |     |  |  +- io.netty:netty-tcnative-boringssl-static:jar:osx-x86_64:2.0.61.Final:compile
 |  |     |  |  +- io.netty:netty-tcnative-boringssl-static:jar:osx-aarch_64:2.0.61.Final:compile
 |  |     |  |  \- io.netty:netty-tcnative-boringssl-static:jar:windows-x86_64:2.0.61.Final:compile
 |  |     |  +- io.projectreactor.netty:reactor-netty-http:jar:1.1.15:compile
 |  |     |  |  +- io.netty:netty-resolver-dns:jar:4.1.105.Final:compile
 |  |     |  |  |  \- io.netty:netty-codec-dns:jar:4.1.105.Final:compile
 |  |     |  |  +- io.netty:netty-resolver-dns-native-macos:jar:osx-x86_64:4.1.105.Final:compile
 |  |     |  |  |  \- io.netty:netty-resolver-dns-classes-macos:jar:4.1.105.Final:compile
 |  |     |  |  \- io.projectreactor.netty:reactor-netty-core:jar:1.1.15:compile
 |  |     |  \- io.netty:netty-common:jar:4.1.105.Final:compile
 |  |     \- com.fasterxml.jackson.dataformat:jackson-dataformat-xml:jar:2.15.3:compile
 |  |        +- org.codehaus.woodstox:stax2-api:jar:4.2.1:compile
 |  |        \- com.fasterxml.woodstox:woodstox-core:jar:6.5.1:compile
 |  +- com.azure.spring:spring-cloud-azure-autoconfigure:jar:5.9.1:compile
 |  |  +- com.azure.spring:spring-cloud-azure-service:jar:5.9.1:compile
 |  |  \- jakarta.validation:jakarta.validation-api:jar:3.0.2:compile
 |  \- com.azure.spring:spring-cloud-azure-resourcemanager:jar:5.9.1:compile
 |     +- com.azure.spring:spring-cloud-azure-core:jar:5.9.1:compile
 |     |  +- com.azure:azure-identity:jar:1.11.1:compile
 |     |  |  +- com.microsoft.azure:msal4j:jar:1.14.0:compile
 |     |  |  |  +- com.nimbusds:oauth2-oidc-sdk:jar:10.7.1:compile
 |     |  |  |  |  +- com.github.stephenc.jcip:jcip-annotations:jar:1.0-1:compile
 |     |  |  |  |  +- com.nimbusds:content-type:jar:2.2:compile
 |     |  |  |  |  +- com.nimbusds:lang-tag:jar:1.7:compile
 |     |  |  |  |  \- com.nimbusds:nimbus-jose-jwt:jar:9.30.2:compile
 |     |  |  |  \- net.minidev:json-smart:jar:2.5.0:compile
 |     |  |  |     \- net.minidev:accessors-smart:jar:2.5.0:compile
 |     |  |  |        \- org.ow2.asm:asm:jar:9.3:compile
 |     |  |  +- com.microsoft.azure:msal4j-persistence-extension:jar:1.2.0:compile
 |     |  |  |  \- net.java.dev.jna:jna:jar:5.13.0:compile
 |     |  |  \- net.java.dev.jna:jna-platform:jar:5.6.0:compile
 |     |  \- com.azure:azure-core-management:jar:1.11.9:compile
 |     \- com.azure.resourcemanager:azure-resourcemanager:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-resources:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-storage:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-compute:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-network:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-keyvault:jar:2.34.0:compile
 |        |  +- com.azure:azure-security-keyvault-keys:jar:4.7.3:compile
 |        |  \- com.azure:azure-security-keyvault-secrets:jar:4.7.3:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-msi:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-sql:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-authorization:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-appservice:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-cosmos:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-containerservice:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-monitor:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-containerregistry:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-dns:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-appplatform:jar:2.34.0:compile
 |        |  \- com.azure:azure-storage-file-share:jar:12.21.1:compile
 |        |     \- com.azure:azure-storage-common:jar:12.24.1:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-containerinstance:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-privatedns:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-redis:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-eventhubs:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-trafficmanager:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-servicebus:jar:2.34.0:compile
 |        +- com.azure.resourcemanager:azure-resourcemanager-cdn:jar:2.34.0:compile
 |        \- com.azure.resourcemanager:azure-resourcemanager-search:jar:2.34.0:compile
 \- org.projectlombok:lombok:jar:1.18.30:compile
```
