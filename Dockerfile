# (svc|api|app|bff)
ARG SERVICE_TYPE=svc

# Extract fat JAR into files
FROM eclipse-temurin:17.0.3_7-jre-alpine as builder
ARG SERVICE_TYPE
ARG JAR_FILE=target/asb-session-test-*.jar
WORKDIR /${SERVICE_TYPE}/
COPY ${JAR_FILE} ${SERVICE_TYPE}.jar
RUN java -Xmx500m -Djarmode=layertools -jar ${SERVICE_TYPE}.jar extract
# Permissions
RUN chmod 500 $(find . -type d)
RUN chmod 400 $(find . -type f)

# Starts from a small JDK 17 image
FROM eclipse-temurin:17.0.3_7-jre-alpine
ARG SERVICE_TYPE
# Create user and set ownership and permissions as required
RUN adduser -D alice
USER alice
# Put service resources in directory named with the service type (svc|api|app|bff)
WORKDIR /${SERVICE_TYPE}/
# Copy extracted files from the previous step
COPY --from=builder --chown=alice:alice ${SERVICE_TYPE}/dependencies/ ./
COPY --from=builder --chown=alice:alice ${SERVICE_TYPE}/spring-boot-loader/ ./
COPY --from=builder --chown=alice:alice ${SERVICE_TYPE}/snapshot-dependencies/ ./
COPY --from=builder --chown=alice:alice ${SERVICE_TYPE}/application/ ./

# Expose port (default 8080 configured in helm deployment)
EXPOSE 8080

# Mount volume which contains configuration files (convention is to use config subdirectory in service directory)
VOLUME /${SERVICE_TYPE}/config

# Timezone
ENV TZ=Europe/Paris

# Start application (it add Java options to the commande line)
ENV SERVICE_TYPE=${SERVICE_TYPE}
ENTRYPOINT java \
           --enable-preview \
           -Djava.security.egd=file:/dev/./urandom \
# https://github.com/Azure/azure-sdk-for-java/issues/30483
-Dreactor.schedulers.defaultBoundedElasticSize=50 \
           -Xms128m \
           -Xmx256m \
           org.springframework.boot.loader.JarLauncher \
           --spring.config.additional-location=file:/${SERVICE_TYPE}/config/
