package eu.ima.testasb.asbsessiontest.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import com.azure.spring.messaging.servicebus.support.ServiceBusMessageHeaders;

import lombok.extern.slf4j.Slf4j;

/**
 * Message consumer beans.
 */
@Service
@Slf4j
public class MessagesConsumers {

	/** Session ID / is a message currently processed for this session. */
	private static final Map<Integer, Integer> currentSessionC;
//	private static final Map<Integer, Integer> currentSessionD;
//	private static Map<Integer, Integer> lastMessageForD;
	static {
		currentSessionC = new HashMap<>();
		currentSessionC.put(MessagesProducer.SESSION_ID_0, null);
		currentSessionC.put(MessagesProducer.SESSION_ID_1, null);
		currentSessionC.put(MessagesProducer.SESSION_ID_2, null);
		currentSessionC.put(MessagesProducer.SESSION_ID_3, null);
		currentSessionC.put(MessagesProducer.SESSION_ID_4, null);
		currentSessionC.put(MessagesProducer.SESSION_ID_5, null);
//		currentSessionD = new HashMap<>();
//		currentSessionD.put(MessagesProducer.SESSION_ID_0, null);
//		currentSessionD.put(MessagesProducer.SESSION_ID_1, null);
//		currentSessionD.put(MessagesProducer.SESSION_ID_2, null);
//		currentSessionD.put(MessagesProducer.SESSION_ID_3, null);
//		currentSessionD.put(MessagesProducer.SESSION_ID_4, null);
//		currentSessionD.put(MessagesProducer.SESSION_ID_5, null);
//		lastMessageForD = new HashMap<>();
//		lastMessageForD.put(MessagesProducer.SESSION_ID_0, -1);
//		lastMessageForD.put(MessagesProducer.SESSION_ID_1, -1);
//		lastMessageForD.put(MessagesProducer.SESSION_ID_2, -1);
//		lastMessageForD.put(MessagesProducer.SESSION_ID_3, -1);
//		lastMessageForD.put(MessagesProducer.SESSION_ID_4, -1);
//		lastMessageForD.put(MessagesProducer.SESSION_ID_5, -1);
	}

//	/** . */
//	private final LockByKey<Integer> lockerForD = new SimpleLockByKey<>();

//	/** Durée d'attente pour la Subscription B. */
//	@Value("${ima.asbsessiontest.subscription-b-delay}")
//	private int subscriptionBDelay;

//	/**
//	 * From a Subscription with 1 session.
//	 *
//	 * @return the consumer
//	 */
//	@Bean
//	public Consumer<Message<Integer>> consumerFromSubscriptionA() {
//		return message -> {
//			final Checkpointer checkpointer = (Checkpointer) message.getHeaders().get(CHECKPOINTER);
//			final int sessionID = getSessionId(message);
//			final int messageId = getMessageId(message);
//			log.debug("ConsumerSubA : Message #{} received with session '{}'", messageId, sessionID);
//			if (checkpointer != null) {
//				log.debug("ConsumerSubA : Message #{} successfully checkpointed", messageId);
//				checkpointer.success() //
//						.doOnSuccess(s -> log.debug("Message '{}' successfully checkpointed", messageId)) //
//						.doOnError(e -> log.error("Error found", e)) //
//						.block();
//			}
//		};
//	}

//	private final int messageRejectedNb = 0;

//	/**
//	 * .
//	 *
//	 * @param messageId
//	 * @param sessionId
//	 */
//	record MessageReceived(int messageId, int sessionId) {
//		//
//	}

//	private final List<MessageReceived> messagesHistory = new ArrayList<>();

//	/**
//	 * From a Subscription with 2 sessions.
//	 *
//	 * @return the consumer
//	 */
//	@Bean
//	public Consumer<Message<Integer>> consumerFromSubscriptionB() {
//		return message -> {
//			final int sessionID = getSessionId(message);
//			final int messageId = getMessageId(message);
//			log.debug("ConsumerSubB : Message #{} received with session '{}'", messageId, sessionID);
//			sleep("ConsumerSubB", subscriptionBDelay);
//			if (messageId == 1 && messageRejectedNb <= 3) {
//				messageRejectedNb++;
//				throw new IllegalArgumentException("Exception volontaire");
//			}
//			messagesHistory.add(new MessageReceived(messageId, sessionID));
//			log.debug("ConsumerSubB : Message #{} consumed with session '{}'", messageId, sessionID);
//			log.info("ConsumerSubB : {} / {}", messagesHistory.size(), messagesHistory);
//		};
//	}

	/**
	 * From a Subscription with 2 sessions / 2 calls.
	 *
	 * @return the consumer
	 */
	@Bean
	public Consumer<Message<Integer>> consumerFromSubscriptionC() {
		return message -> {
			final int sessionID = getSessionId(message);
			final int currentMessageId = getMessageId(message);
			log.debug("ConsumerSubC : Message #{} received with session '{}'", currentMessageId, sessionID);
			// We check if this consumer is already processing a message for this session
			final Integer alreadyConsumingMessageId = currentSessionC.get(sessionID);
			if (alreadyConsumingMessageId != null) {
				log.error("ConsumerSubC : Already processing message #{} for session {} and received #{}: {}",
						alreadyConsumingMessageId, sessionID, currentMessageId, currentSessionC);
			}
			// On ralenti volontairement un peu ce consommateur car on l'a configuré
			// avec 2 max-concurrent-calls. On verra donc bien dans les logs si on a 2
			// traces séquentielles ou parallèles (grâce aux messages de début et fin de
			// traitement: problème si on a 2 début et 2 fin, OK si début/fin/début/fin).
			currentSessionC.put(sessionID, currentMessageId);
			sleep("ConsumerSubC", 100);
			currentSessionC.put(sessionID, null);
			log.debug("ConsumerSubC : Message #{} consumed with session '{}'", currentMessageId, sessionID);
		};
	}

//	/**
//	 * From a Subscription with 3 sessions / 3 calls.
//	 *
//	 * @return the consumer
//	 */
//	@Bean
//	public Consumer<Message<Integer>> consumerFromSubscriptionD() {
//		return message -> {
//			final int sessionID = getSessionId(message);
//			lockerForD.lock(sessionID);
//			try {
//				final int currentMessageId = getMessageId(message);
//				log.debug("ConsumerSubD : Message #{} received with session '{}'", currentMessageId, sessionID);
//				if (currentMessageId < lastMessageForD.get(sessionID)) {
//					log.error("ConsumerSubD : Bad order: received message #{} before #{} for session {}",
//							currentMessageId, lastMessageForD.get(sessionID), sessionID);
//				}
//				lastMessageForD.put(sessionID, currentMessageId);
//				// We check if this consumer is already processing a message for this session
//				final Integer alreadyConsumingMessageId = currentSessionD.get(sessionID);
//				if (alreadyConsumingMessageId != null) {
//					log.error("ConsumerSubD : Already processing message #{} for session {}: {}",
//							alreadyConsumingMessageId, sessionID, currentSessionC);
//				}
//				// On ralenti volontairement un peu ce consommateur car on l'a configuré
//				// avec 3 max-concurrent-calls. On verra donc bien dans les logs si on a 2
//				// traces séquentielles ou parallèles (grâce aux messages de début et fin de
//				// traitement: problème si on a 2 début et 2 fin, OK si début/fin/début/fin).
//				currentSessionD.put(sessionID, currentMessageId);
//				sleep("ConsumerSubD", 100);
//				currentSessionD.put(sessionID, null);
//				log.debug("ConsumerSubD : Message #{} consumed with session '{}'", currentMessageId, sessionID);
//			} finally {
//				lockerForD.unlock(sessionID);
//			}
//		};
//	}

//	/**
//	 * @return the consumer
//	 */
//	@Bean
//	public Consumer<Message<Integer>> consumerFromQueue() {
//		return message -> {
//			final int sessionID = getSessionId(message);
//			log.debug("ConsumerQueue: Message #{} received with session '{}'", message.getPayload(), sessionID);
//		};
//	}

	private static int getMessageId(final Message<Integer> message) {
		return message.getPayload();
	}

	private static int getSessionId(final Message<?> message) {
		final Object rawSessionID = message.getHeaders().get(ServiceBusMessageHeaders.SESSION_ID);
		return Integer.parseInt((String) rawSessionID);
	}

	private static void sleep(final String entityName, final int ms) {
		try {
			Thread.sleep(ms);
		} catch (final InterruptedException excpt) {
			log.error("{}: Thread interrupted: {} (cause: {})", entityName, excpt.getMessage(), Optional
					.ofNullable(excpt.getCause()).map(Throwable::getClass).map(Class::getSimpleName).orElse("?"));
			// Thread.currentThread().interrupt();
			// throw new RuntimeException(excpt);
		}
	}

}
