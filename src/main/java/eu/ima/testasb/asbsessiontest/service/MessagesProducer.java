package eu.ima.testasb.asbsessiontest.service;

import java.time.OffsetDateTime;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.azure.spring.messaging.servicebus.support.ServiceBusMessageHeaders;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Sinks;

/**
 * Messages producer.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class MessagesProducer {

	/** Session IDs. */
	public static final int SESSION_ID_0 = 1000;
	public static final int SESSION_ID_1 = 1001;
	public static final int SESSION_ID_2 = 1002;
	public static final int SESSION_ID_3 = 1003;
	public static final int SESSION_ID_4 = 1004;
	public static final int SESSION_ID_5 = 1005;

	/** Le sink de publication. */
	private final Sinks.Many<Message<Integer>> many;

	/** ID du message, incrémenté à chaque publication. */
	private final AtomicInteger lastMessageId = new AtomicInteger(0);

	/**
	 * Lance une publication de message à intervalle régulier.
	 */
	@Scheduled(initialDelay = 5L, fixedDelayString = "${ima.asbsessiontest.producer.scheduled-delay-seconds}", timeUnit = TimeUnit.SECONDS)
	public void scheduleFixedDelayTask() {
		if (many != null) {
			// 1 message pour les session #1000 et #1002
			doSend(SESSION_ID_0);
			doSend(SESSION_ID_2);
			// 2 messages pour les session #1001, #1003, #1004 et #1005
			doSend(SESSION_ID_1);
			doSend(SESSION_ID_1);
			doSend(SESSION_ID_3);
			doSend(SESSION_ID_3);
			doSend(SESSION_ID_4);
			doSend(SESSION_ID_4);
			doSend(SESSION_ID_5);
			doSend(SESSION_ID_5);
		}
	}

	private void doSend(final int sessionId) {
		final int messageId = lastMessageId.incrementAndGet();
//		if (messageId >= -1) {
//			// if (messageId <>= 2501) {
//			return;
//		}
		final Message<Integer> msg = MessageBuilder //
				.withPayload(messageId) //
				.setHeader("asbsessiontest-msg-id", messageId) //
				.setHeader("asbsessiontest-date", OffsetDateTime.now().toString()) //
				.setHeader(ServiceBusMessageHeaders.SESSION_ID, sessionId) //
				.build();
		try {
			Thread.sleep(100);
		} catch (final InterruptedException excpt) {
			//
		}

		log.debug("Producer     : Send message #{} with session '{}'", messageId, sessionId);
		many.emitNext(msg, (signalType, emitResult) -> {
			// En cas d'erreur
			log.error("EmitFailureHandler received a '{}' signal: {}", signalType, emitResult);
			// return true if the operation should be retried, false otherwise
			return true;
		});
	}

	/**
	 * Méthode appelée lors d'un problème de publication / consommation dans le bus
	 * de messages. <br/>
	 * On va extraire les données du message d'erreur Spring reçu en paramétre.
	 *
	 * @param errMsg le message d'erreur Spring
	 */
	@ServiceActivator(inputChannel = "errorChannel")
	public void errorHandler(final ErrorMessage errMsg) {
		log.error("Error handler called with {}", errMsg);
	}

	/**
	 * @return l'ID du dernier message publié
	 */
	public int getLastMessageId() {
		return lastMessageId.get();
	}

}
