package eu.ima.testasb.asbsessiontest.lock;

/**
 * .
 *
 * @param <K> le type de la clé. Attention à ce que cette classe implémente
 *            correctement {@link Object#equals(Object)} et
 *            {@link Object#hashCode()} afin de pouvoir comparer 2 clés ensemble
 */
public interface LockByKey<K> {

	/**
	 * Cette méthode retourne lorsque un accès exclusif est accordé au thread
	 * appelant. Tout les autres threads demandant un accès avec cette même clé
	 * seront bloqués. Attention à bien appeler {@link #unlock(Object)} dans un
	 * try-catch-finally à la fin du traitement.
	 *
	 * @param key la clé
	 */
	void lock(K key);

	/**
	 * Libère l'accès exclusif. Si un autre thread est actuellement en attente pour
	 * cette même clé, il sera libéré.
	 *
	 * @param key la clé
	 */
	void unlock(K key);

}