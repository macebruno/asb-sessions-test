package eu.ima.testasb.asbsessiontest.lock;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Cette implémentation utilise une {@link ConcurrentHashMap} pour lister les
 * clés en cours avec un {@link Lock} associé à chaque clé. <br/>
 * Le petit plus est que l'on garde un compteur sur le nombre de threads en
 * attente d'un verrou.
 *
 * @param <K> le type de la clé
 */
public class SimpleLockByKey<K> implements LockByKey<K> {

	/**
	 * Liste des clés/verrou. Une entrée dans cette Map indique qu'au moins 1 thread
	 * est en cours de verrou sur cette clé. On utilise bien une
	 * {@link ConcurrentHashMap} pour nous garantir que les accès sont thread-safe
	 * (à condiion d'utiliser les méthodes atomiques de cette classe ensuite...).
	 */
	private final ConcurrentHashMap<K, LockWithCounter> currentLocks = new ConcurrentHashMap<>();

	/**
	 * .
	 */
	private static class LockWithCounter {

		/**
		 * Le verrou pour la clé. On utilise un {@link Lock} standard Java.
		 */
		private final Lock lock = new ReentrantLock();

		/**
		 * Nombre de threads pour ce verrou, donc au moins 1 en cours + éventuellement
		 * d'autres en attente. Si ce compteur est à 0, cet objet peut éventuellement
		 * être recyclé.
		 */
		private final AtomicInteger numberOfThreadsInQueue = new AtomicInteger(1);

		/**
		 * .
		 *
		 * @return
		 */
		private LockWithCounter incrementWaitingThreadCounter() {
			numberOfThreadsInQueue.incrementAndGet();
			return this;
		}

		/**
		 * .
		 *
		 * @return le nombre de threads en attente
		 */
		private int decrementThreadCounter() {
			return numberOfThreadsInQueue.decrementAndGet();
		}

	}

	/**
	 * @see LockByKey#lock(Object)
	 */
	@Override
	public void lock(final K key) {
		final LockWithCounter lock = currentLocks.compute(key,
				(k, v) -> v == null ? new LockWithCounter() : v.incrementWaitingThreadCounter());
		lock.lock.lock();
	}

	/**
	 * @see LockByKey#unlock(Object)
	 */
	@Override
	public void unlock(final K key) {
		final LockWithCounter lock = currentLocks.get(key);
		lock.lock.unlock();
		currentLocks.compute(key, (k, v) -> v.decrementThreadCounter() == 0 ? null : v);
//		if (lock.decrementThreadCounter() == 0) {
//			currentLocks.remove(key, lock);
//		}
	}

}
