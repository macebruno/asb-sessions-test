package eu.ima.testasb.asbsessiontest.conf;

import java.util.function.Supplier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

/**
 * Configuration du producteur de messages.
 */
@Configuration
@EnableScheduling
@Slf4j
public class ServiceProducerConfiguration {

	/**
	 * @return le sink de publication
	 */
	@Bean
	public Sinks.Many<Message<Integer>> many() {
		return Sinks.many().unicast().onBackpressureBuffer();
	}

	/**
	 * .
	 *
	 * @param sink le sink de publication
	 * @return le flux de message
	 */
	@Bean
	public Supplier<Flux<Message<Integer>>> producer(final Sinks.Many<Message<Integer>> sink) {
		return () -> sink.asFlux() //
				// .retryWhen(Retry.backoff(30, Duration.ofSeconds(2))) //
				.doOnNext(m -> log.trace("Manually sending message {}", m)) //
				.doOnError(this::doOnError);
	}

	private void doOnError(final Throwable t) {
		// Jamais appelé ?
		log.error("doOnError: {}", t.getMessage(), t);
	}

}
