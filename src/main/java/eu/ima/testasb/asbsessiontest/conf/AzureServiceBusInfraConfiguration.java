package eu.ima.testasb.asbsessiontest.conf;

import java.time.Duration;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;

import com.azure.core.exception.ResourceNotFoundException;
import com.azure.core.http.HttpClient;
import com.azure.core.http.HttpResponse;
import com.azure.core.http.ProxyOptions;
import com.azure.core.util.Configuration;
import com.azure.core.util.ConfigurationBuilder;
import com.azure.core.util.HttpClientOptions;
import com.azure.identity.ClientSecretCredential;
import com.azure.identity.ClientSecretCredentialBuilder;
import com.azure.messaging.servicebus.administration.ServiceBusAdministrationClient;
import com.azure.messaging.servicebus.administration.ServiceBusAdministrationClientBuilder;
import com.azure.messaging.servicebus.administration.implementation.models.ServiceBusManagementErrorException;
import com.azure.messaging.servicebus.administration.models.CreateQueueOptions;
import com.azure.messaging.servicebus.administration.models.CreateRuleOptions;
import com.azure.messaging.servicebus.administration.models.CreateSubscriptionOptions;
import com.azure.messaging.servicebus.administration.models.CreateTopicOptions;
import com.azure.messaging.servicebus.administration.models.EmptyRuleAction;
import com.azure.messaging.servicebus.administration.models.SqlRuleFilter;
import com.azure.messaging.servicebus.administration.models.TopicProperties;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;

/**
 * At the application startup: create the Subscriptions and Queues required.
 */
@Slf4j
@org.springframework.context.annotation.Configuration
public class AzureServiceBusInfraConfiguration {

	/** Default rule name of a Subscription. */
	private static final String DEFAULT_RULE_NAME = "$Default";

	/** Connexion à l'Azure Service Bus. */
	@Value("${spring.cloud.azure.servicebus.connection-string:#{null}}")
	private Optional<String> serviceBusConnectionString;
	@Value("${spring.cloud.azure.credential.client-id:#{null}}")
	private Optional<String> serviceBusClientId;
	@Value("${spring.cloud.azure.credential.client-secret:#{null}}")
	private Optional<String> serviceBusClientSecret;
	@Value("${spring.cloud.azure.profile.tenant-id:#{null}}")
	private Optional<String> serviceBusTenantId;
	@Value("${spring.cloud.azure.servicebus.namespace:#{null}}")
	private Optional<String> serviceBusNamespace;

	/** Nom du Topic. */
	@Value("${ima.asbsessiontest.topic-name}")
	private String mainTopicName;

//	/** Nom de la Subscription. */
//	@Value("${spring.cloud.stream.bindings.consumerFromSubscriptionA-in-0.group}")
//	private String subscriptionAName;

//	/** Nom de la Subscription. */
//	@Value("${spring.cloud.stream.bindings.consumerFromSubscriptionB-in-0.group}")
//	private String subscriptionBName;

	/** Nom de la Subscription. */
	@Value("${spring.cloud.stream.bindings.consumerFromSubscriptionC-in-0.group}")
	private String subscriptionCName;

//	/** Nom de la Subscription. */
//	@Value("${spring.cloud.stream.bindings.consumerFromSubscriptionD-in-0.group}")
//	private String subscriptionDName;

//	/** Nom de la Queue. */
//	@Value("${spring.cloud.stream.bindings.consumerFromQueue-in-0.destination}")
//	private String queueEName;

	/** Le client Azure Service Bus. */
	private ServiceBusAdministrationClient client;

	/**
	 * Créé les Subscriptions / Queues manquantes au démarrage de l'application.
	 */
	@PostConstruct
	public void createSubscriptions() {
		log.info("Start of Subscriptions / Queues creation");
		// Connexion à l'Azure Service Bus
		if (serviceBusConnectionString.isPresent()) {
			log.info("Connexion via une 'connection string'");
			final Configuration conf = Configuration.getGlobalConfiguration();
			client = new ServiceBusAdministrationClientBuilder() //
					.connectionString(serviceBusConnectionString.get()) //
					.configuration(conf) //
					.buildClient();
		} else {
			log.info("Connexion via un 'service principal'");
			final Configuration conf = new ConfigurationBuilder() //
					.putProperty(Configuration.PROPERTY_NO_PROXY, "localhost|*.servicebus.windows.net") //
					.putProperty(Configuration.PROPERTY_HTTP_PROXY, "http://localhost:3128") //
					.putProperty(Configuration.PROPERTY_HTTPS_PROXY, "http://localhost:3128") //
					.putProperty("http.proxy.non-proxy-hosts", "localhost|*.servicebus.windows.net") //
					.putProperty("http.proxy.hostname", "localhost") //
					.putProperty("http.proxy.port", "3128") //
					.build();
			final HttpClientOptions httpClientOptions = new HttpClientOptions();
			httpClientOptions.setProxyOptions(ProxyOptions.fromConfiguration(conf));
			final HttpClient httpClient = com.azure.core.http.HttpClient.createDefault(httpClientOptions);
			final ClientSecretCredential credential = new ClientSecretCredentialBuilder() //
					.clientId(serviceBusClientId.get()) //
					.clientSecret(serviceBusClientSecret.get()) //
					.tenantId(serviceBusTenantId.get()) //
					.httpClient(httpClient) //
					.build();
			client = new ServiceBusAdministrationClientBuilder() //
					.credential(serviceBusNamespace.get() + ".servicebus.windows.net", credential) //
					.configuration(conf) //
					.buildClient();
		}
		// Création Topic
		ensureTopicExists(mainTopicName);
		// Une Subscription sans consommateur, juste pour garder trace des messages
//		ensureSubscriptionExists(mainTopicName, "Z_all_msgs");
		// Des Subscriptions avec plus ou moins de consommateurs dessus
//		ensureSubscriptionExists(mainTopicName, subscriptionAName);
//		ensureSubscriptionExists(mainTopicName, subscriptionBName);
		ensureSubscriptionExists(mainTopicName, subscriptionCName);
//		ensureSubscriptionExists(mainTopicName, subscriptionDName);
		// Une Subscription avec un forward vers une Queue
//		ensureQueueExists(queueEName);
//		ensureSubscriptionExists(mainTopicName, "E_forward_queue", queueEName);
		log.info("Subscriptions / Queues created");
		log.info("");
	}

	private boolean ensureTopicExists(final String topicName) {
		// Suppression au cas ou il existerai déjà
		try {
			log.debug("Deleting Topic '{}'...", topicName);
			client.deleteTopic(topicName);
		} catch (final ServiceBusManagementErrorException | ResourceNotFoundException excpt) {
			try (final HttpResponse resp = excpt.getResponse()) {
				if (resp.getStatusCode() != 404) {
					throw excpt;
				}
			}
		}
		// Création
		final CreateTopicOptions options = new CreateTopicOptions();
		options.setAutoDeleteOnIdle(Duration.ofHours(2));
		options.setDefaultMessageTimeToLive(Duration.ofHours(1));
		options.setMaxDeliveryCount(5);
		options.setSessionRequired(true);
		options.setUserMetadata("Topic de tests de sessions");
		log.debug("Creating Topic '{}'...", topicName);
		final TopicProperties props = client.createTopic(topicName, options);
		props.getStatus();
		return true;
	}

	private boolean ensureSubscriptionExists(final String topicName, final String subscriptionName) {
		return ensureSubscriptionExists(topicName, subscriptionName, (String) null);
	}

	private boolean ensureQueueExists(final String queueName) {
		final CreateQueueOptions opts = new CreateQueueOptions();
		// C'est un projet de test - on purge la Queue après 2h
		opts.setAutoDeleteOnIdle(Duration.ofHours(2));
		opts.setDeadLetteringOnMessageExpiration(true);
		opts.setDefaultMessageTimeToLive(Duration.ofHours(12));
		opts.setMaxDeliveryCount(5);
		opts.setSessionRequired(true);
		return ensureQueueExists(queueName, opts);
	}

	private boolean ensureSubscriptionExists( //
			final String topicName, //
			final String subscriptionName, //
			final String forwardTo) {
		final CreateSubscriptionOptions options = new CreateSubscriptionOptions();
		// C'est un projet de test - on purge la Subscription après 3h
		options.setAutoDeleteOnIdle(Duration.ofHours(3));
		// C'est un projet de test - on purge les messages après 2h
		options.setDefaultMessageTimeToLive(Duration.ofHours(2));
		// Session requise
		options.setSessionRequired(forwardTo == null);
		// Max 5 tentatives de livraison
		options.setMaxDeliveryCount(5);
		// Dead-letter dans tout les cas d'erreur
		options.setDeadLetteringOnMessageExpiration(true);
		options.setEnableDeadLetteringOnFilterEvaluationExceptions(true);
		// Forward si demandé
		if (forwardTo != null) {
			options.setForwardTo(forwardTo);
		}
		return ensureSubscriptionExists(topicName, subscriptionName, options);
	}

	private boolean ensureSubscriptionExists( //
			final String topicName, //
			final String subscriptionName, //
			final CreateSubscriptionOptions subscriptionOptions) {
		// Filtre sur tout les messages
		final SqlRuleFilter filter = new SqlRuleFilter("1=1");
		// Que l'on accepte
		final CreateRuleOptions ruleOptions = new CreateRuleOptions(filter);
		ruleOptions.setAction(new EmptyRuleAction());
		// Créé la Subscription
		return ensureSubscriptionExists(topicName, subscriptionName, subscriptionOptions, ruleOptions);
	}

	private boolean ensureSubscriptionExists( //
			final String topicName, //
			final String subscriptionName, //
			final CreateSubscriptionOptions subscriptionOptions, //
			final CreateRuleOptions rule) {
		final String ruleName = "MessagesFilter";
		// Suppression dans tout les cas
		deleteSubscription(topicName, subscriptionName);
		// Création Subscription
		log.debug("Creating subscription '{}/{}'", topicName, subscriptionName);
		client.createSubscription(topicName, subscriptionName, subscriptionOptions);
		// Ajout du filtre
		log.debug("Creating rule '{}/{}/{}'", topicName, subscriptionName, ruleName);
		client.createRule(topicName, ruleName, subscriptionName, rule);
		// Suppression du filtre ajouté par défaut lors de la création de Subscription
		log.debug("Deleting default rule '{}/{}'", topicName, subscriptionName);
		client.deleteRule(topicName, subscriptionName, DEFAULT_RULE_NAME);
		return false;
	}

	private boolean ensureQueueExists( //
			final String queueName, //
			final CreateQueueOptions queueOptions) {
		// Suppression dans tout les cas
		deleteQueue(queueName);
		// Création Queue
		log.debug("Creating queue '{}'", queueName);
		client.createQueue(queueName, queueOptions);
		return false;
	}

	private void deleteSubscription(final String topicName, final String subscriptionName) {
		log.warn("Deleting subscription '{}/{}'", topicName, subscriptionName);
		if (isSubscriptionExists(topicName, subscriptionName)) {
			return;
		}
		try {
			client.deleteSubscription(topicName, subscriptionName);
		} catch (final ServiceBusManagementErrorException | ResourceNotFoundException excpt) {
			try (final HttpResponse resp = excpt.getResponse()) {
				final int httpStatusCode = resp.getStatusCode();
				// Ignore une 404 - on considère que le DELETE est idempotent
				if (httpStatusCode != 404) {
					throw excpt;
				}
			}
		}
	}

	private boolean isSubscriptionExists(final String topicName, final String subscriptionName) {
		try {
			return client.getSubscriptionExists(topicName, subscriptionName);
		} catch (final ServiceBusManagementErrorException | ResourceNotFoundException excpt) {
			try (final HttpResponse resp = excpt.getResponse()) {
				final int httpStatusCode = resp.getStatusCode();
				// Ignore une 404 - on considère que le DELETE est idempotent
				if (httpStatusCode == 404) {
					return false;
				}
				throw excpt;
			}
		}
	}

	private void deleteQueue(final String queueName) {
		log.warn("Deleting queue '{}'", queueName);
		try {
			client.deleteQueue(queueName);
		} catch (final ServiceBusManagementErrorException | ResourceNotFoundException excpt) {
			try (final HttpResponse resp = excpt.getResponse()) {
				final int httpStatusCode = resp.getStatusCode();
				// Ignore une 404 - on considère que le DELETE est idempotent
				if (httpStatusCode != 404) {
					throw excpt;
				}
			}
		}
	}

}
