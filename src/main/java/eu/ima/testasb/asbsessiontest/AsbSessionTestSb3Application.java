package eu.ima.testasb.asbsessiontest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Point d'entrée de l'application.
 */
@SpringBootApplication
public class AsbSessionTestSb3Application {

	/**
	 * Point d'entrée de l'application.
	 *
	 * @param args les arguments de la ligne de commande
	 */
	public static void main(final String... args) {
		SpringApplication.run(AsbSessionTestSb3Application.class, args);
	}

}
